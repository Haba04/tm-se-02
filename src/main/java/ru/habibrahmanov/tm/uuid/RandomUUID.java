package ru.habibrahmanov.tm.uuid;

import java.util.UUID;

public class RandomUUID {

    public String genRandomUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
