package ru.habibrahmanov.tm.manager;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.uuid.RandomUUID;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ProjectManager {
    private List<Project> projectsList = new ArrayList<>();
    RandomUUID randomUUID = new RandomUUID();
    private Project curProject;

    public Project getCurProject() {
        return curProject;
    }

    public void setCurProject(Project curProject) {
        this.curProject = curProject;
    }

    Scanner scanner = new Scanner(System.in);

    public Project createProject(String name) {
        Project project = new Project(name, randomUUID.genRandomUUID());
        projectsList.add(project);
        curProject = project;
        return project;
    }

    public void projectList() {
        if (projectsList.isEmpty()) {
            System.out.println("LIST EMPTY");
        } else {
            for (int i = 0; i < projectsList.size(); i++) {
                System.out.println("PROJECT ID № " + projectsList.get(i).getId() + " : NAME " + projectsList.get(i).getName());
            }
        }
    }

    public void clearProjects() {
        projectsList.clear();
    }

    public void removeProject(String projectId) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId.equals(projectsList.get(i).getId())) {
                System.out.println("entity project " + projectsList.get(i).getName() + " is removed");
                projectsList.remove(projectsList.get(i));
            }
        }
    }

    public void editProject(String projectId, String name) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId.equals(projectsList.get(i).getId())) {
                projectsList.get(i).setName(name);
                System.out.println("entity.Project id " + projectsList.get(i).getId() + " renamed, new name: " + projectsList.get(i).getName());
            }
        }
    }

    public void switchProject (String projectId) {
        for (int i = 0; i < projectsList.size(); i++) {
            if (projectId.equals(projectsList.get(i).getId())) {
                curProject = projectsList.get(i);
                System.out.println("CURRENT PROJECT ID " + projectId);
            }
        }
    }
}

