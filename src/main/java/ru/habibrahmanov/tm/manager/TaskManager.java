package ru.habibrahmanov.tm.manager;

import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.uuid.RandomUUID;
import java.util.Scanner;

public class TaskManager {
    Scanner scanner = new Scanner(System.in);
    RandomUUID randomUUID = new RandomUUID();

    public Task addTask(Project curProject) {
        Task task = new Task(scanner.nextLine(), randomUUID.genRandomUUID());
        curProject.getTasks().add(task);
        System.out.println("TASK ID: " + task.getId() + " ADDED TO PROJECT ID: " + curProject.getId());
        return task;
    }


    public void taskList(Project curProject) {
        if (curProject.getTasks().isEmpty()) {
            System.out.println("LIST EMPTY");
        } else {
            for (int i = 0; i < curProject.getTasks().size(); i++) {
                System.out.println("TASK ID № " + curProject.getTasks().get(i).getId() + " : NAME " + curProject.getTasks().get(i).getName());
            }
        }
    }

    public void removeTask(Project curProject, String taskId) {
        for (int i = 0; i < curProject.getTasks().size(); i++) {
            if (taskId.equals(curProject.getTasks().get(i).getId())) {
                System.out.println("entity task " + curProject.getTasks().get(i).getName() + " is removed");
                curProject.getTasks().remove(curProject.getTasks().get(i));
            }
        }
    }

    public void editTask(Project curProject, String taskId) {
        for (int i = 0; i < curProject.getTasks().size(); i++) {
            if (taskId.equals(curProject.getTasks().get(i).getId())) {
                System.out.println("ENTER NEW NAME");
                curProject.getTasks().get(i).setName(scanner.nextLine());
                System.out.println("entity Task id " + curProject.getTasks().get(i).getId() + " renamed, new name: " + curProject.getTasks().get(i).getName());
            }
        }
    }

}

