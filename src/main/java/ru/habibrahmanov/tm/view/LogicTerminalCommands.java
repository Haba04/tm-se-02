package ru.habibrahmanov.tm.view;


import ru.habibrahmanov.tm.manager.ProjectManager;
import ru.habibrahmanov.tm.manager.TaskManager;

import java.util.Scanner;

class LogicTerminalCommands {

    private ProjectManager projectManager;
    private TaskManager taskManager;
    private Scanner scanner;

    LogicTerminalCommands(ProjectManager projectManager, TaskManager taskManager, Scanner scanner) {
        this.projectManager = projectManager;
        this.taskManager = taskManager;
        this.scanner = scanner;
    }

    void projectCreate() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        projectManager.createProject(scanner.nextLine());
        System.out.println("[OK]");
    }

    void projectList() {
        System.out.println("[PROJECT LIST]");
        projectManager.projectList();
    }

    void projectRemove() {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID PROJECT:");
        projectManager.removeProject(scanner.nextLine());
        System.out.println("[OK]");
    }

    void projectEdit() {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER ID");
        String projectId = scanner.nextLine();
        System.out.println("ENTER NAME");
        String name = scanner.nextLine();
        projectManager.editProject(projectId, name);
    }

    void projectClear() {
        System.out.println("[PROJECT CLEAR]");
        projectManager.clearProjects();
    }

    void taskCreate() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME");
        taskManager.addTask(projectManager.getCurProject());
    }

    void taskList() {
        System.out.println("[TASK LIST]");
        taskManager.taskList(projectManager.getCurProject());
    }

    void taskRemove() {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER ID TASK:");
        String taskId = scanner.nextLine();
        taskManager.removeTask(projectManager.getCurProject(), taskId);
    }

    void taskEdit() {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER ID TASK:");
        String taskIdEd = scanner.nextLine();
        taskManager.editTask(projectManager.getCurProject(), taskIdEd);
    }

    void projectSwitch() {
        System.out.println("[PROJECT SWITCH]");
        System.out.println("ENTER PROJECT ID");
        projectManager.switchProject(scanner.nextLine());
    }

    void projectHelp() {
        System.out.println("help: Show all commands");
        System.out.println("[pcl] project-clear: Remove all project");
        System.out.println("[pr] project-remove: Remove selected project");
        System.out.println("[pcr] project-create: Create new projects");
        System.out.println("[pl] project-list: Shaw all project");
        System.out.println("[ps] project-switch: Switch current project");
        System.out.println("[tcr] task-create: Create new task");
        System.out.println("[tl] task-list: Show all tasks");
        System.out.println("[tr] task-remove: Remove selected task");
        System.out.println("[te] task-edit: Edit selected task");
    }

}
