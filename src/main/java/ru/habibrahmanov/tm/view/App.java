package ru.habibrahmanov.tm.view;

import ru.habibrahmanov.tm.manager.ProjectManager;
import ru.habibrahmanov.tm.manager.TaskManager;

import java.util.*;

public class App {

    public static void main(String[] args) {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter HELP for show all commands.");
        ProjectManager projectManager = new ProjectManager();
        TaskManager taskManager = new TaskManager();
        Scanner scanner = new Scanner(System.in);

        LogicTerminalCommands logicTerminalCommands = new LogicTerminalCommands(projectManager, taskManager, scanner);

        String sc;

        do {
            sc = scanner.nextLine();

            switch (sc.toLowerCase()) {
                case (TerminalCommands.PROJECTCREATE):
                case (TerminalCommands.PCR):
                    logicTerminalCommands.projectCreate();
                    break;

                case (TerminalCommands.PROJECTLIST):
                case (TerminalCommands.PL):
                    logicTerminalCommands.projectList();
                    break;

                case (TerminalCommands.PROJECTREMOVE):
                case (TerminalCommands.PR):
                    logicTerminalCommands.projectRemove();
                    break;

                case (TerminalCommands.PROJECTEDIT):
                case (TerminalCommands.PE):
                    logicTerminalCommands.projectEdit();
                    break;

                case (TerminalCommands.PROJECLEAR):
                case (TerminalCommands.PCL):
                    logicTerminalCommands.projectClear();
                    break;

                case (TerminalCommands.PROJECTSWITCH):
                case (TerminalCommands.PS):
                    logicTerminalCommands.projectSwitch();
                    break;

                case (TerminalCommands.TASKCREATE):
                case (TerminalCommands.TCR):
                    logicTerminalCommands.taskCreate();
                    break;

                case (TerminalCommands.TASKCLIST):
                case (TerminalCommands.TL):
                    logicTerminalCommands.taskList();
                    break;

                case (TerminalCommands.TASKCREMOVE):
                case (TerminalCommands.TR):
                    logicTerminalCommands.taskRemove();
                    break;

                case (TerminalCommands.TASKCEDIT):
                case (TerminalCommands.TE):
                    logicTerminalCommands.taskEdit();
                    break;

                case (TerminalCommands.HELP):
                    logicTerminalCommands.projectHelp();
                    break;

            }
        } while (!sc.equals("exit"));



    }


}
