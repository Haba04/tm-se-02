package ru.habibrahmanov.tm.view;


class TerminalCommands {

    static final String PROJECTCREATE = "project-create";
    static final String PCR = "pcr";

    static final String PROJECTLIST = "project-list";
    static final String PL = "pl";

    static final String PROJECTREMOVE = "project-remove";
    static final String PR = "pr";

    static final String PROJECTEDIT = "project-edit";
    static final String PE = "pe";

    static final String PROJECLEAR = "project-clear";
    static final String PCL = "pcl";

    static final String PROJECTSWITCH = "project-switch";
    static final String PS = "ps";

    static final String TASKCREATE = "task-create";
    static final String TCR = "tcr";

    static final String TASKCLIST = "task-list";
    static final String TL = "tl";

    static final String TASKCREMOVE = "task-remove";
    static final String TR = "tr";

    static final String TASKCEDIT = "task-edit";
    static final String TE = "te";

    static final String HELP = "help";

}
